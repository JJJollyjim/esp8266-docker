FROM buildpack-deps:stretch

RUN apt-get update && apt-get install -y build-essential flex bison gperf python-serial

RUN curl -L 'https://dl.espressif.com/dl/xtensa-lx106-elf-linux64-1.22.0-92-g8facf4c-5.2.0.tar.gz' | tar zxf -
ENV PATH ${PATH}:/xtensa-lx106-elf/bin

RUN curl -L https://github.com/espressif/ESP8266_RTOS_SDK/archive/master.tar.gz | tar zxf -
ENV IDF_PATH=/ESP8266_RTOS_SDK-master